#include <REGX52.H>
#include "LCD1602.H"
void main()
{
	LCD_Init();//用之前必须要初始化
	
	LCD_ShowChar(1,1,'C');//1行1列，显示字符c
	
	LCD_ShowString(1,3,"cheng");//从1行3列开始显示字符串cheng
	
	LCD_ShowNum(1,9,521,3);//从1行9列开始3个位显示数字 521，如果4个位的话用0补齐，0521
 
	LCD_ShowSignedNum(1,14,-66,2);//从1行14列开始2个位显示数字 -66
	
	LCD_ShowHexNum(2,1,0xFF,2);//从2行1列开始2个位显示16进制数字 FF
	
	LCD_ShowBinNum(2,4,0xFF,8);//从2行4列开始8个位显示16进制数字 1111 1111
	
	while(1)
		{
		
		}
}
